package firstapp.sourceit.com.homework1task3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity {
    EditText login;
    EditText email;
    EditText phoneNumber;
    EditText password;
    EditText passwordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.login);
        email = (EditText) findViewById(R.id.email);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        password = (EditText) findViewById(R.id.password);
        passwordConfirm = (EditText) findViewById(R.id.passwordConfirm);
        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().length() == 0) {
                    login.setError("Login is required");
                    email.requestFocus();
                } else if (!isValidEmail(email.getText().toString())) {
                    email.setError("Email is not valid");
                    email.requestFocus();
                } else if (isValidPhone(phoneNumber.getText().toString())) {
                    phoneNumber.setError("Invalid Phone number");
                    phoneNumber.requestFocus();
                } else if (!isValidPassword(password.getText().toString())) {
                    password.setError("Invalid Password");
                    password.requestFocus();
                } else if (!isValidPasswordConfirm(passwordConfirm.getText().toString())) {
                    passwordConfirm.setError("Password doesn't match");
                    passwordConfirm.requestFocus();
                } else {
                    Toast.makeText(MainActivity.this, "Input Validation Success", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isValidEmail(String email) {
        final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidPhone(String phoneNumber) {
        boolean check;
        if (phoneNumber.length() < 6 || phoneNumber.length() > 13) {
            check = false;
        } else {
            check = true;
        }
        return check;
    }

    private boolean isValidPassword(String password) {
        if (password != null && password.length() > 9) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidPasswordConfirm(String passwordConfirm) {
        if (passwordConfirm != null) {
            return true;
        } else {
            return false;
        }
    }

}
